<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new animal("shaun");

echo "Nama Hewan : $object->name <br>";
echo "Jumlah Kaki : $object->legs <br>";
echo "Cold blooded animal : $object->cold_blooded <br> <br>";

$object2 = new frog("buduk");

echo "Nama Hewan : $object2->name <br>";
echo "Jumlah Kaki : $object2->legs <br>";
echo "Cold blooded animal : $object2->cold_blooded <br>";
$object2->jump();
echo "<br>";
echo "<br>";

$object3 = new ape("kera sakti");

echo "Nama Hewan : $object3->name <br>";
echo "Jumlah Kaki : $object3->legs <br>";
echo "Cold blooded animal : $object3->cold_blooded <br>";
$object3->yell();